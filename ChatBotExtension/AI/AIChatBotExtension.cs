﻿/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using ESPGatewayExtension;
using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Logging;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Events;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Requests;

namespace ChatBotExtension.AI
{
    [Export(typeof(IESPExtension))]
    [ExportMetadata("Service", "AIChatBot")]
    class AIChatBotExtension : ChatBotExtension
    {
        private Dictionary<string, UserInfo> chatParties = new Dictionary<string, UserInfo>();
        private int chatAgents = 0;

        private IChatAIEngine chatAIEngine = null;
        private bool chatStopOnAgent = true;

        private CompositionContainer _container;

        [ImportMany]
        IEnumerable<Lazy<IChatAIEngine, IChatAIEngineData>> chatAIEngines = null;

        /// <summary>
        /// This constructor will be called by MEF to instanciate a singleton
        /// </summary>
        [ImportingConstructor]
        public AIChatBotExtension()
        {
        }

        /// <summary>
        /// This constructor will be called by the MEF singleton to instanciate a unique instance
        /// for each ESP request received.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="method"></param>
        /// <param name="requestContext"></param>
        protected AIChatBotExtension(String service, String method, IRequestContext requestContext, IChatAIEngine chatAIEngine) : base(service, method, requestContext)
        {
            Logger.Info("Creating AIChatBot Instance");

            string ChatScriptId = (string)espParameters.GetAsString("ChatScriptId");
            if (ChatScriptId == null || ChatScriptId == "")
                SendError(requestContext, "100", "ChatScriptId parameter is absent");

            if (!Boolean.TryParse((string)espParameters.GetAsString("StopOnAgent"), out chatStopOnAgent))
            {
                chatStopOnAgent = true;
            }

            this.chatAIEngine = chatAIEngine;
            this.chatAIEngine.Initialize(ChatScriptId, Logger);
        }

        private IChatAIEngine CreateChatAIEngine(string method, ILogger logger)
        {
            foreach(var chatAIEngine in chatAIEngines)
            {
                if (chatAIEngine.Metadata.AIEngine.Equals(method))
                {
                    return chatAIEngine.Value;
                }
            }

            Logger.Error("Error: Unable to find ChatAIEngine for ESP method '" + method + "'");

            return null;
        }

        public override void Initialize(KeyValueCollection appConfiguration)
        {
            base.Initialize(appConfiguration);

            //An aggregate catalog that combines multiple catalogs
            var catalog = new AggregateCatalog();

            //Adds all the parts found in the same assembly as the Program class
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(AIChatBotExtension).Assembly));

            string path = Environment.GetCommandLineArgs()[0];
            string dir = "Extensions";
            try
            {
                dir = appConfiguration.GetAsKeyValueCollection("settings").GetAsString("extensions-dir");
            }
            catch { }
            string extensionsDir = Path.GetDirectoryName(path) + "\\" + dir;
            //Adds all the parts found in the 'Extensions' folder
            catalog.Catalogs.Add(new DirectoryCatalog(extensionsDir));

            //Create the CompositionContainer with the parts in the catalog
            _container = new CompositionContainer(catalog);

            //Fill the imports of this object
            try
            {
                this._container.ComposeParts(this);
            }
            catch (CompositionException compositionException)
            {
                Console.WriteLine(compositionException.ToString());
            }
        }

        public override IESPExtension HandleRequest(string service, string method, IRequestContext requestContext)
        {
            // The singleton instance of the ChatBotExtension has the container of AIEngines
            // so resolve the engine to use now and hand it to the ChatBot instance.
            IChatAIEngine chatAIEngine = CreateChatAIEngine(method, null);

            // Each Chat needs a unique ChatBot instance.  So we'll use the MEF instanciated
            // ChatBotExtension as a Class Factory to spawn those instances.
            AIChatBotExtension instance = new AIChatBotExtension(service, method, requestContext, chatAIEngine);
            return instance;
        }

        protected override void HandleMessages(IMessage message)
        {
            base.HandleMessages(message);

            EventSessionInfo eventSessionInfo = (EventSessionInfo)message;

            if (eventSessionInfo.ChatTranscript == null)
                return;

            foreach (var chatEvent in eventSessionInfo.ChatTranscript.ChatEventList)
            {
                Logger.Debug("ChatEvent: " + chatEvent.ToString());

                if (chatEvent is MessageInfo)
                {
                    MessageInfo messageInfo = chatEvent as MessageInfo;

                    if ( !messageInfo.UserId.Equals(chatUserIdSelf) )
                    {
                        string response = chatAIEngine.GetNextMessage(messageInfo.MessageText.Text, null);

                        if (response != null)
                        {
                            Logger.Debug("Sending AI response: " + response);
                            SendResponse(response);
                        }
                        else // script finished diconnecting ...
                        {
                            LeaveAndDisconnect();
                        }
                    }
                }
                else if (chatEvent is NewPartyInfo)
                {
                    NewPartyInfo newParty = (NewPartyInfo)chatEvent;
                    UserInfo userInfo = newParty.UserInfo;
                    chatParties.Add(((NewPartyInfo)chatEvent).UserId, userInfo);

                    if (UserType.Agent == userInfo.UserType && chatUserIdSelf != newParty.UserId)
                        chatAgents++;  // count all agents involved in chat, except for ourselves

                    if ( chatAgents > 0 && chatStopOnAgent )
                    {
                        Logger.Debug("Disconnecting from chat because agent has joined");
                        LeaveAndDisconnect();
                    }
                }
                else if (chatEvent is PartyLeftInfo)
                {
                    PartyLeftInfo eventPartyLeft = chatEvent as PartyLeftInfo;

                    // Ignore ourselves leaving the chat
                    if (chatUserIdSelf == eventPartyLeft.UserId)
                        continue;

                    bool disconnect = false;
                    UserInfo userInfo;
                    if (!chatParties.TryGetValue(eventPartyLeft.UserId, out userInfo) || userInfo == null)
                    {
                        disconnect = true; // means that client is leaving (we assume that agents joined after the chatbot joined)
                    }
                    else if (userInfo.UserType == UserType.Agent)
                    {
                        chatAgents--;  // we need to count all agents
                        if (chatAgents == 0) // i.e. last agent is leaving
                        {
                            if (eventPartyLeft.Reason.Code != 0) // i.e. not Action.KeepAlive 
                                disconnect = true;
                        }
                    }

                    if (disconnect)
                    {
                        Logger.Debug("Disconnecting from chat because client has left");
                        LeaveAndDisconnect();
                    }
                }
            }
        }

        protected void LeaveAndDisconnect()
        {
            Logger.Info("Logout of chat session and disconnect from chat server");

            RequestReleaseParty requestReleaseParty = RequestReleaseParty.Create();
            requestReleaseParty.SessionId = chatSessionId;
            requestReleaseParty.UserId = chatServerProtocol.UserId;
            requestReleaseParty.AfterAction = Genesyslab.Platform.WebMedia.Protocols.BasicChat.Action.CloseIfNoAgents;
            Logger.Info("AIChatBot leaving chat session " + chatSessionId + " as userId=" + chatUserIdSelf);
            IMessage respondingEvent = chatServerProtocol.Request(requestReleaseParty, new TimeSpan(0, 0, 30));

            chatServerProtocol.Close();
        }
    }
}
