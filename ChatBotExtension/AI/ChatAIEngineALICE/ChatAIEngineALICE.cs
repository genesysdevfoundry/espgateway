/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.ComponentModel.Composition;
using AIMLbot;
using Genesyslab.Platform.Commons.Logging;

namespace ChatBotExtension.AI.ChatAIEngineALICE
{
    /**
     * endless chatscriot that use A.L.I.C.E.-baset chat bot engine
     */
    [Export(typeof(IChatAIEngine))]
    [ExportMetadata("AIEngine", "ALICE")]
    public class ChatAIEngineALICE: IChatAIEngine
    {
        private Bot m_bot;
        private User m_user;
        private ILogger logger;
        
        #region IChatAIEngine Members

        public void Initialize(string scriptId, ILogger logger)
        {
            this.logger = logger;

            m_bot = new Bot();
            m_bot.loadSettings(scriptId);

            AIMLbot.Utils.AIMLLoader loader = new AIMLbot.Utils.AIMLLoader(m_bot);
            m_bot.isAcceptingUserInput = false;
            loader.loadAIML(m_bot.PathToAIML);
            m_bot.isAcceptingUserInput = true;
        }

        public string GetNextMessage(string userInput, string userName)
        {
            if (m_user == null)
                if (!Load(userName))
                    return null;

            if (userInput == null)
                userInput = "Hi";

            Request myRequest = new Request(userInput, m_user, m_bot);
            Result myResult = m_bot.Chat(myRequest);

            return myResult.Output;
        }

        #endregion

        private bool Load(string userName)
        {
            if (userName == null)
                userName = "Unknown";

            try
            {
                m_user = new User(userName, m_bot);

                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Exception in ChatAIEngineALICE" + ex.Message);
                return false;
            }
        }
    }
}
