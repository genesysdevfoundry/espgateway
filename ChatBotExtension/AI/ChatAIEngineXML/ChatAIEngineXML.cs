/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Xml;
using Genesyslab.Platform.Commons.Logging;

namespace ChatBotExtension.AI.ChatAIEngineALICE
{
    [Export(typeof(IChatAIEngine))]
    [ExportMetadata("AIEngine", "XML")]
    public class ChatAIEngineXML: IChatAIEngine
    {
        private ILogger logger;
        private List<string> messages = new List<string>();
        private int currentMessagePosition = 0;

        #region IChatAIEngine Members

        public void Initialize(string scriptId, ILogger logger)
        {
            this.logger = logger;
            string xmlChatFilename = scriptId + ".xml";

            try
            {   // read and load document
                logger.Info("Reading ChatScript file'" + xmlChatFilename + "'");

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlChatFilename);
                XmlNodeList nodeList = xmlDoc.SelectNodes("//Message");
                foreach(var node in nodeList)
                {
                    messages.Add(((XmlElement)node).InnerText);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error while parsing '" + scriptId + "': "+e.ToString());
            }
            
            logger.Info("ChatScript file'" + scriptId + "': " +messages.Count+"messages read");
        }

        public string  GetNextMessage(string userInput, string userName)
        {
            string message = null;

            if (currentMessagePosition < messages.Count)
                message = messages[currentMessagePosition++];

            return message;
        }

        #endregion
    }

}
