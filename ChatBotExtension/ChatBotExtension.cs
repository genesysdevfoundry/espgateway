﻿/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Threading;
using ESPGatewayExtension;
using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols.ExternalService.Event;
using Genesyslab.Platform.OpenMedia.Protocols.ExternalService.Request;
using Genesyslab.Platform.WebMedia.Protocols;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Events;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Requests;

namespace ChatBotExtension
{
    public class ChatBotExtension : ESPExtension
    {
        protected BasicChatProtocol chatServerProtocol;
        protected string chatSessionId;
        protected string chatUserIdSelf;

        protected string chatNickname = "ChatBot";

        private Thread messageReceiver;

        public ChatBotExtension()
        {
        }

        protected ChatBotExtension(String service, String method, IRequestContext requestContext)
        {
            Logger.Debug("ChatBotExtension: Calling base.HandleRequest");
            Logger.Debug(requestContext.ToString());

            // This constructor creates a unique ESPExtension for each ESP request.
            // In order to initialize the ESPExtension base class properly, we need
            // to call the HandleRequest method to do so.
            base.HandleRequest(service, method, requestContext);

            Logger.Debug("ChatBotExtension: Extracting ChatServerHost from UserData");
            string chatServerHost = espUserdata.GetAsString("ChatServerHost");
            if (chatServerHost == null || chatServerHost == "")
                SendError(requestContext, "100", "'ChatServerHost' key is missing from UserData");

            Logger.Debug("ChatBotExtension: Extracting ChatServerPort from UserData");
            string chatServerPort = espUserdata.GetAsString("ChatServerPort");
            if (chatServerPort == null || chatServerPort == "")
                SendError(requestContext, "100", "'ChatServerPort' key is missing from UserData");

            Logger.Debug("ChatBotExtension: Extracting InteractionId from UserData");
            string chatSessionId = espUserdata.GetAsString("InteractionId");
            if (chatSessionId == null || chatSessionId == "")
                SendError(requestContext, "100", "'InteractionId' key is missing from UserData");

            Logger.Debug("ChatBotExtension: Extracting Nickname from Parameters");
            string nickname = espParameters.GetAsString("Nickname");
            Logger.Debug("Nickname in parameters: " + nickname);
            if (!string.IsNullOrEmpty(nickname))
            {
                Logger.Debug("Setting Nickname property to " + nickname);
                this.chatNickname = nickname;
            }

            Logger.Info(string.Format("Connecting to ChatServer Host[{0}] Port[{1}] SessionId[{2}]", chatServerHost, chatServerPort, chatSessionId ));
            Connect(chatServerHost, chatServerPort, chatSessionId);
        }

        #region ESPExtension
        public override void Initialize(KeyValueCollection appConfiguration)
        {
            Logger.Info("ChatBotExtension: Initializing");
            base.Initialize(appConfiguration);
            Logger.Info("ChatBotExtension: Initialized");
        }

        public override IESPExtension HandleRequest(String service, String method, IRequestContext requestContext)
        {
            Logger.Info("ChatBotExtension: Handling ESP request for service:'" + service + "'" + " method:'" + method + "'.");

            ChatBotExtension instance = null;
            try
            {
                // Each Chat needs a unique ChatBot instance.  So we'll use the MEF instanciated
                // ChatBotExtension as a Class Factory to spawn those instances.
                instance = new ChatBotExtension(service, method, requestContext);
            }
            catch (Exception exc)
            {
                Logger.Error("Exception Handling Request in SampleExtension: " + exc.Message);
            }

            return instance;
        }

        public override void Terminate()
        {
            Logger.Info("SampleExtension: Terminating");

            Logger.Info("SampleExtension: Terminated");
        }
        #endregion

        protected void Connect(string chatServerHost, string chatServerPort, string session)
        {
            chatSessionId = session;
            Uri chatServerUri = new Uri("tcp://" + chatServerHost + ":" + chatServerPort);

//            chatServerProtocol = new BasicChatProtocol(new Endpoint("endpoints:101", chatServerUri));
            chatServerProtocol = new BasicChatProtocol(new Endpoint("ESPGateway_ChatBotExtension", chatServerUri));

            //Chat Server Event handlers
            chatServerProtocol.Opened += ChatServerProtocol_Opened;
            chatServerProtocol.Closed += ChatServerProtocol_Closed;
            chatServerProtocol.Error += ChatServerProtocol_Error;

            chatServerProtocol.UserNickname = chatNickname;
            chatServerProtocol.UserType = UserType.Agent;
            Logger.Info("Connecting to Chat Server: " + chatServerUri.ToString());

            try
            {
                chatServerProtocol.Open();
            }
            catch (Exception ex)
            {
                Logger.Error("Error connecting to Chat Server: " + ex.Message);
            }
        }

        private void ChatServerProtocol_Error(object sender, EventArgs e)
        {
            
        }

        private void ChatServerProtocol_Opened(object sender, EventArgs e)
        {
            try
            {
                Logger.Info("Connected to Chat Server");

                messageReceiver = new Thread(new ThreadStart(this.ReceiveMessages));
                messageReceiver.Start();
                Join();
            }
            catch (Exception exc)
            {
                Logger.Error("Error starting message receiver and joining chat: " + exc.Message);
            }
        }

        private void ChatServerProtocol_Closed(object sender, EventArgs e)
        {
            Logger.Info("Disconnected from Chat Server ");
        }

        protected void Join()
        {
            try
            {
                Logger.Info("Joining chat session '" + chatSessionId + "'");

                RequestJoin requestJoin = RequestJoin.Create();
                requestJoin.SessionId = chatSessionId;

                IMessage respondingEvent = chatServerProtocol.Request(requestJoin, new TimeSpan(0, 0, 30));

                if (respondingEvent == null)
                    return;

                if (respondingEvent.Name.Equals("EventSessionInfo"))
                {
                    chatUserIdSelf = chatServerProtocol.UserId;
                    Logger.Info("Joined chat session =" + chatSessionId + " as userId=" + chatUserIdSelf);
                }

                if (respondingEvent.Id == EventSessionInfo.MessageId)
                {
                    //doEventSessionInfo((EventSessionInfo)respondingEvent);
                }

            }
            catch (ProtocolException protocolException)
            {
                Logger.Error("Protocol Exception!\n" + protocolException);
            }
        }

        private void ReceiveMessages()
        {
            Logger.Info("Starting ReceiveMessages Thread!");

            try
            {
                while (chatServerProtocol.State != ChannelState.Closed)
                {
                    IMessage message = chatServerProtocol.Receive();
                    // Call a user-defined function that does the actual work:
                    if (message != null)
                    {
                        Logger.Debug("ReceiveMessages: New message received: \n\r" + message.ToString());
                        if (message.Id == EventSessionInfo.MessageId)
                            HandleMessages(message);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Logger.Error("Error: " + ex.ToString());
            }

            Logger.Info("Exiting ReceiveMessages Thread!");
        }

        virtual protected void HandleMessages(IMessage message)
        {
        }

        protected void SendResponse(string response)
        {
            try
            {
                RequestMessage requestMessage = RequestMessage.Create();

                requestMessage.MessageText = MessageText.Create(MsgCheck.None, "", TreatAs.NORMAL, response);
                requestMessage.SessionId = chatSessionId;

                IMessage respondingEvent = chatServerProtocol.Request(requestMessage, new TimeSpan(0, 0, 30));

                if (respondingEvent != null)
                    Logger.Debug(respondingEvent.ToString());
            }
            catch (Exception e)
            {
                Logger.Debug("Exception Sending Response: " + e.Message);
            }
        }
    }
}
