﻿/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using ESPGatewayExtension;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Events;
using Genesyslab.Platform.WebMedia.Protocols.BasicChat.Requests;

namespace ChatBotExtension
{
    [Export(typeof(IESPExtension))]
    [ExportMetadata("Service", "EchoChatBot")]
    public class EchoChatBotExtension : ChatBotExtension
    {
        private Dictionary<string, UserInfo> chatParties = new Dictionary<string, UserInfo>();
        private int chatAgents = 0;

        /// <summary>
        /// This constructor will be called by MEF to instanciate a singleton
        /// </summary>
        [ImportingConstructor]
        public EchoChatBotExtension()
        {
        }

        /// <summary>
        /// This constructor will be called by the MEF singleton to instanciate a unique instance
        /// for each ESP request received.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="method"></param>
        /// <param name="requestContext"></param>
        protected EchoChatBotExtension(String service, String method, IRequestContext requestContext) : base(service, method, requestContext)
        {
            Logger.Info("Creating EchoChatBot Instance");
        }

        public override IESPExtension HandleRequest(string service, string method, IRequestContext requestContext)
        {
            Logger.Info("EchoChatBotExtension: Handling ESP request for service:'" + service + "'" + " method:'" + method + "'.");
            Logger.Info(requestContext.ToString());

            // Each Chat needs a unique ChatBot instance.  So we'll use the MEF instanciated
            // ChatBotExtension as a Class Factory to spawn those instances.
            EchoChatBotExtension instance = new EchoChatBotExtension(service, method, requestContext);
            return instance;
        }

        protected override void HandleMessages(IMessage message)
        {
            base.HandleMessages(message);

            EventSessionInfo eventSessionInfo = (EventSessionInfo)message;

            if (eventSessionInfo.ChatTranscript == null)
                return;

            foreach (var chatEvent in eventSessionInfo.ChatTranscript.ChatEventList)
            {
                Logger.Debug("ChatEvent: " + chatEvent.ToString());

                if (chatEvent is MessageInfo)
                {
                    MessageInfo messageInfo = chatEvent as MessageInfo;

                    if ( !messageInfo.UserId.Equals(chatUserIdSelf) )
                    {
                        string msg = messageInfo.MessageText.Text;

                        if (!string.IsNullOrWhiteSpace(msg))
                        {
                            SendResponse(msg);
                        }
                    }
                }
                else if (chatEvent is NewPartyInfo)
                {
                    NewPartyInfo newParty = (NewPartyInfo)chatEvent;
                    UserInfo userInfo = newParty.UserInfo;
                    chatParties.Add(((NewPartyInfo)chatEvent).UserId, userInfo);

                    if (UserType.Agent == userInfo.UserType && chatUserIdSelf != newParty.UserId)
                        chatAgents++;  // count all agents involved in chat, except for ourselves
                }
                else if ( chatEvent is PartyLeftInfo)
                {
                    PartyLeftInfo eventPartyLeft = (PartyLeftInfo)chatEvent;

                    // Ignore ourselves leaving the chat
                    if (chatUserIdSelf == eventPartyLeft.UserId)
                        continue;

                    bool disconnect = false;
                    UserInfo userInfo;
                    if (!chatParties.TryGetValue(eventPartyLeft.UserId, out userInfo) || userInfo == null)
                    {
                        disconnect = true; // means that client is leaving (we assume that agents joined after the chatbot joined)
                    }
                    else if (userInfo.UserType == UserType.Agent)
                    {
                        chatAgents--;  // we need to count all agents
                        if (chatAgents == 0) // i.e. last agent is leaving
                        {
                            if (eventPartyLeft.Reason.Code != 0) // i.e. not Action.KeepAlive 
                                disconnect = true;
                        }
                    }

                    if (disconnect)
                    {
                        RequestReleaseParty requestReleaseParty = RequestReleaseParty.Create();
                        requestReleaseParty.SessionId = chatSessionId;
                        requestReleaseParty.UserId = chatServerProtocol.UserId;
                        requestReleaseParty.AfterAction = Genesyslab.Platform.WebMedia.Protocols.BasicChat.Action.CloseIfNoAgents;
                        Logger.Info("EchoChatBot leaving chat session " + chatSessionId + " as userId=" + chatUserIdSelf);
                        IMessage respondingEvent = chatServerProtocol.Request(requestReleaseParty, new TimeSpan(0, 0, 30));
                    }
                }
            }
        }
    }
}
