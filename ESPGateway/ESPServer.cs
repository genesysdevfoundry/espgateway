﻿/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Net;
using ESPGatewayExtension;
using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Logging;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols.ExternalService.Event;
using Genesyslab.Platform.OpenMedia.Protocols.ExternalService.Request;

namespace ESPGateway
{
    public class ESPServer
    {
        public static ILogger logger;

        KeyValueCollection appConfiguration = null;
        public ExternalServiceProtocolListener listener;
        private int ESP_PORT = 7855;

        private CompositionContainer _container;

        [ImportMany]
        IEnumerable<Lazy<IESPExtension, IESPExtensionData>> extensions = null;

        public ESPServer(KeyValueCollection appConfiguration)
        {
            this.appConfiguration = appConfiguration;

            string logPath = ".\\ESPGateway";
            try
            {
                logPath = appConfiguration.GetAsKeyValueCollection("log").GetAsString("all");
            }
            catch { }
            String now = String.Format("{0:s}", DateTime.Now);
            now = now.Replace('-', '_');
            now = now.Replace(':', '_');

            string logFileName = String.Format("{0}_{1}.log", logPath, now);

            string logLevel = "INFO";
            try
            {
                logLevel = appConfiguration.GetAsKeyValueCollection("log").GetAsString("verbose");
            }
            catch { }

            logger = new FileLogger(logLevel, logFileName);
            ESPExtensionLogger.Logger = logger;
        }

        bool m_shutdowned = false;
        private bool IsShutdown()
        {
            return m_shutdowned;
        }

        private void LoadExtensions()
        {
            //An aggregate catalog that combines multiple catalogs
            var catalog = new AggregateCatalog();

            //Adds all the parts found in the same assembly as the Program class
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Program).Assembly));

            string path = Environment.GetCommandLineArgs()[0];
            string dir = "Extensions";
            try
            {
                dir = appConfiguration.GetAsKeyValueCollection("settings").GetAsString("extensions-dir");
            }
            catch { }
            string extensionsDir = Path.GetDirectoryName(path) + "\\" + dir;

            //Adds all the parts found in the 'Extensions' folder
            catalog.Catalogs.Add(new DirectoryCatalog(extensionsDir));

            //Create the CompositionContainer with the parts in the catalog
            _container = new CompositionContainer(catalog);

            //Fill the imports of this object
            try
            {
                this._container.ComposeParts(this);
            }
            catch (CompositionException compositionException)
            {
                logger.Error(compositionException.ToString());
            }
        }

        public void Initialize()
        {
            LoadExtensions();

            foreach (Lazy<IESPExtension, IESPExtensionData> i in extensions)
            {
                i.Value.Initialize(appConfiguration);
            }

            Int32.TryParse(appConfiguration.GetAsString("server-port"), out ESP_PORT);

            listener = new ExternalServiceProtocolListener(new Endpoint(Dns.GetHostName(), Dns.GetHostName(), ESP_PORT));
            listener.EnableLogging(logger);
            listener.Opened += Listener_Opened;
            listener.Closed += Listener_Closed;
            listener.Error += Listener_Error;
            listener.ClientChannelOpened += Listener_ClientChannelOpened;
            listener.ClientChannelClosed += Listener_ClientChannelClosed;
        }

        private void Listener_Opened(object sender, EventArgs e)
        {
            logger.Info("Port Opened for listening");
        }

        private void Listener_Closed(object sender, EventArgs e)
        {
            logger.Info("Port Closed for listening");
        }

        private void Listener_Error(object sender, EventArgs e)
        {
            logger.Info("Error received on listening port");
        }

        private void Listener_ClientChannelOpened(object sender, EventArgs e)
        {
            logger.Info("Interaction Server connected");
        }

        private void Listener_ClientChannelClosed(object sender, EventArgs e)
        {
            logger.Info("Interaction Server disconnected");
        }

        public void Terminate()
        {
            foreach (Lazy<IESPExtension, IESPExtensionData> i in extensions)
            {
                i.Value.Terminate();
            }
        }

        public void Run()
        {
            logger.Info("Message Receiver Thread Started");

            try
            {
                logger.Debug("Opening ESP Port: " + ESP_PORT.ToString());
                listener.Open();
                IRequestContext request=null;
                while (!IsShutdown())
                {
                    try
                    {
                        request = listener.ReceiveRequest();
                        if (request != null)
                        {
                            IMessage o = request.RequestMessage;
                            Request3rdServer espReq = o as Request3rdServer;

                            if (!espReq.Request.ContainsKey("Service"))
                            {
                                logger.Error("ERROR Service is not specified in ESP request.");
                                throw new ESPException("101", "Service is not specified");
                            }

                            Dispatch(espReq.Request.GetAsString("Service"),
                                     espReq.Request.GetAsString("Method"),
                                     request);
                        }

                        SendOk(request);
                    }
                    catch (ESPException e)
                    {
                        logger.Error("Exception Dispatching Message to Extension: " + e.ToString());
                        SendError(request, e._code, e._message);
                    }
                    catch(Exception e)
                    {
                        logger.Error("Exception Handling Request: " + e.ToString());
                        SendError(request, "999", e.Message);
                    }
                }

                logger.Debug("Closing ESP Port: " + ESP_PORT.ToString());
                listener.Close();
            }
            catch (Exception e)
            {
                logger.Error("Run Exception:" + e.StackTrace);
            }

            logger.Info("Message Receiver Thread Exiting");
        }

        private void Dispatch(String service, String method, IRequestContext requestContext)
        {
            logger.Info("ESP request to service:'" + service + "'" + " method:'" + method + "' is received.");

            foreach (Lazy<IESPExtension, IESPExtensionData> extension in extensions)
            {
                if (extension.Metadata.Service.Equals(service))
                {
                    logger.Debug("Service '" + service + "' found, dispatching request...");
                    extension.Value.HandleRequest(service, method, requestContext);
                    return;
                }
            }

            logger.Info("Unable to find Extension to handle service:'" + service + "'" + " method:'" + method + "'.");
            throw new ESPException("333", "Not supported");
        }

        private void SendOk(IRequestContext request)
        {
            if (request != null)
            {
                try
                {
                    Request3rdServer espReq = request.RequestMessage as Request3rdServer;

                    Event3rdServerResponse eventOk = Event3rdServerResponse.Create(new KeyValueCollection());

                    eventOk.ReferenceId = espReq.ReferenceId;
                    eventOk.Request.Add("Service", espReq.Request.GetAsString("Service"));
                    eventOk.Request.Add("Method", espReq.Request.GetAsString("Method"));

                    request.Respond(eventOk);
                }
                catch (Exception e)
                {
                    logger.Error("SendOk Exception:" + e.ToString() + ":\n" + e.InnerException.StackTrace);
                }
            }
        }
        private void SendError(IRequestContext request, string errorCode, string errorMessage)
        {
            if (request!=null)
            try
            {
                Request3rdServer espReq = request.RequestMessage as Request3rdServer;

                Event3rdServerFault eventError = Event3rdServerFault.Create(new KeyValueCollection());
                eventError.ReferenceId = espReq.ReferenceId;
                eventError.Request.Add("Service", espReq.Request.GetAsString("Service"));
                eventError.Request.Add("Method", espReq.Request.GetAsString("Method"));

                KeyValueCollection param = new KeyValueCollection();
                param.Add("FaultCode", errorCode);
                param.Add("FaultString", errorMessage);

                eventError.Request.Add("Parameters", param);
                request.Respond(eventError);
            }
            catch (Exception e)
            {
                logger.Error("SendError Exception:" + e.ToString() + ":\n" + e.InnerException.StackTrace);
            }
        }
    }
}
