/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.IO;
using Genesyslab.Platform.Commons.Logging;

namespace ESPGateway
{
	/// <summary>
	/// Summary description for FileLogger.
	/// </summary>
	public class FileLogger : ILogger
	{
		/// <summary>
		///  Typecode for debugging messages. 
		/// </summary>
		public const int LEVEL_DEBUG = 0;

		/// <summary>
		///  String name for debug level messages.
		/// </summary>
		public const string LEVEL_DEBUG_NAME = "DEBUG";

		/// <summary>
		/// Typecode for informational messages. 
		/// </summary>
		public const int LEVEL_INFO = 1;

		/// <summary>
		///  String name for info level messages.
		/// </summary>
		public const string LEVEL_INFO_NAME = "INFO";

		/// <summary>
		/// Typecode for warning messages.
		/// </summary>
		public const int LEVEL_WARN = 2;

		/// <summary>
		///  String name for warn level messages.
		/// </summary>
		public const string LEVEL_WARN_NAME = "WARN";

		/// <summary>
		/// Typecode for error messages.
		/// </summary>
		public const int LEVEL_ERROR = 3;

		/// <summary>
		///  String name for error level messages.
		/// </summary>
		public const string LEVEL_ERROR_NAME = "ERROR";

		/// <summary>
		/// Typecode for fatal error messages.
		/// </summary>
		public const int LEVEL_FATAL = 4;

		/// <summary>
		///  String name for fatal error level messages.
		/// </summary>
		public const string LEVEL_FATAL_NAME = "FATAL ERROR";

		/// <summary>
		/// Typecode for disabled log levels.
		/// </summary>
		public const int LEVEL_DISABLED = 5;

		private int logLevel;
		private string logFile;

        private StreamWriter sw;

		/// <summary>
		/// Creates a new ConsoleLogger with the priority set to DEBUG.
		/// </summary>
		public FileLogger(string sLogFile): this(LEVEL_DEBUG, sLogFile)
		{
		}

        /// <summary>
        /// Creates a new ConsoleLogger.
        /// </summary>
        /// <param name="logLevelName">The Log level name.</param>
        public FileLogger(string logLevelName, string sLogFile)
        {
            switch(logLevelName.ToUpper())
            {
                case "DEBUG": this.logLevel = 0; break;
                case "INFO": this.logLevel = 1; break;
                case "WARN": this.logLevel = 2; break;
                case "ERROR": this.logLevel = 3; break;
                case "FATAL": this.logLevel = 4; break;
                case "DISABLE": this.logLevel = 5; break;
                default: this.logLevel = 1; break;
            }
            this.logFile = sLogFile;

            this.sw = new StreamWriter(logFile, true);
        }

        /// <summary>
        /// Creates a new ConsoleLogger.
        /// </summary>
        /// <param name="logLevel">The Log level typecode.</param>
        public FileLogger(int logLevel, string sLogFile)
		{
			this.logLevel = logLevel;
			this.logFile = sLogFile;

            this.sw = new StreamWriter(logFile, true);
		}

		/// <summary>
		/// Logs a debug message.
		/// </summary>
		/// <param name="message">The Message</param>
        public void Debug(object message)
		{
			Debug(message, null as Exception);
		}

		/// <summary>
		/// Logs a debug message. 
		/// </summary>
		/// <param name="message">The Message</param>
		/// <param name="exception">The Exception</param>
		public void Debug(object message, Exception exception)
		{
			Log(LEVEL_DEBUG, LEVEL_DEBUG_NAME, message, exception); 
		}

		/// <summary>
		/// Logs a debug message.
		/// </summary>
		/// <param name="format">Message format</param>
		/// <param name="args">Array of objects to write using format</param>
		public void DebugFormat( string format, params Object[] args )
		{
			Debug(String.Format(format, args));
		}

		/// <summary>
		/// Determines if messages of priority "debug" will be logged.
		/// </summary>
		/// <value>True if "debug" messages will be logged.</value> 
		public bool IsDebugEnabled
		{
			get
			{
				return (logLevel <= LEVEL_DEBUG);
			}
		}

		/// <summary>
		/// Logs an info message.
		/// </summary>
		/// <param name="message">The Message</param>
		public void Info( object message )
		{
			Info(message, null as Exception);
		}

		/// <summary>
		/// Logs an info message. 
		/// </summary>
		/// <param name="message">The Message</param>
		/// <param name="exception">The Exception</param>
		public void Info( object message, Exception exception)
		{
			Log(LEVEL_INFO, LEVEL_INFO_NAME, message, exception); 
		}

		/// <summary>
		/// Logs an info message.
		/// </summary>
		/// <param name="format">Message format</param>
		/// <param name="args">Array of objects to write using format</param>
		public void InfoFormat( string format, params Object[] args )
		{
			Info(String.Format(format, args));
		}

		/// <summary>
		/// Determines if messages of priority "info" will be logged.
		/// </summary>
		/// <value>True if "info" messages will be logged.</value>
		public bool IsInfoEnabled
		{
			get
			{
				return (logLevel <= LEVEL_INFO);
			}
		}

		/// <summary>
		/// Logs a warn message.
		/// </summary>
		/// <param name="message">The Message</param>
		public void Warn(object message )
		{
			Warn(message, null as Exception);
		}

		/// <summary>
		/// Logs a warn message. 
		/// </summary>
		/// <param name="message">The Message</param>
		/// <param name="exception">The Exception</param>
		public void Warn(object message, Exception exception)
		{
			Log(LEVEL_WARN, LEVEL_WARN_NAME, message, exception); 
		}

		/// <summary>
		/// Logs an warn message.
		/// </summary>
		/// <param name="format">Message format</param>
		/// <param name="args">Array of objects to write using format</param>
		public void WarnFormat( string format, params Object[] args )
		{
			Warn(String.Format(format, args));
		}

		/// <summary>
		/// Determines if messages of priority "warn" will be logged.
		/// </summary>
		/// <value>True if "warn" messages will be logged.</value>
		public bool IsWarnEnabled
		{
			get
			{
				return (logLevel <= LEVEL_WARN);
			}
		}

		/// <summary>
		/// Logs an error message.
		/// </summary>
		/// <param name="message">The Message</param>
		public void Error(object message )
		{
			Error(message, null as Exception);
		}

		/// <summary>
		/// Logs an error message. 
		/// </summary>
		/// <param name="message">The Message</param>
		/// <param name="exception">The Exception</param>
		public void Error(object message, Exception exception)
		{
			Log(LEVEL_ERROR, LEVEL_ERROR_NAME, message, exception); 
		}
		/// <summary>
		/// Logs an error message.
		/// </summary>
		/// <param name="format">Message format</param>
		/// <param name="args">Array of objects to write using format</param>
		public void ErrorFormat( string format, params Object[] args )
		{
			Error(String.Format(format, args));
		}

		/// <summary>
		/// Determines if messages of priority "error" will be logged.
		/// </summary>
		/// <value>True if "error" messages will be logged.</value>
		public bool IsErrorEnabled
		{
			get
			{
				return (logLevel <= LEVEL_ERROR);
			}
		}

		/// <summary>
		/// Logs a fatal error message.
		/// </summary>
		/// <param name="message">The Message</param>
		public void FatalError(object message )
		{
			FatalError(message, null as Exception);
		}

		/// <summary>
		/// Logs a fatal error message.
		/// </summary>
		/// <param name="message">The Message</param>
		/// <param name="exception">The Exception</param>
		public void FatalError(object message, Exception exception)
		{
			Log(LEVEL_FATAL, LEVEL_FATAL_NAME, message, exception); 
		}

		/// <summary>
		/// Logs a fatal error message.
		/// </summary>
		/// <param name="format">Message format</param>
		/// <param name="args">Array of objects to write using format</param>
		public void FatalErrorFormat( string format, params Object[] args )
		{
			FatalError(String.Format(format, args));
		}

		/// <summary>
		/// Determines if messages of priority "fatalError" will be logged.
		/// </summary>
		/// <value>True if "fatalError" messages will be logged.</value>
		public bool IsFatalErrorEnabled
		{
			get 
			{
				return (logLevel <= LEVEL_FATAL); 
			}
		}

		/// <summary>
		/// A Common method to log.
		/// </summary>
		/// <param name="level">The level of logging</param>
		/// <param name="levelName">The Level name</param>
		/// <param name="message">The Message</param>
		/// <param name="exception">The Exception</param>
		protected void Log(int level, string levelName, object message, Exception exception) 
		{
			if(logLevel <= level)
			{
				lock(typeof(FileLogger))
				{
                    DateTime nowDateStr = DateTime.Now;
					//StreamWriter sw = new StreamWriter(logFile, true);
                    sw.WriteLine(string.Format("{0:s}:[{1}] {2}", nowDateStr, levelName, message));
                    Console.Out.WriteLine(string.Format("{0:s}:[{1}] {2}", nowDateStr, levelName, message));
				
					if(exception != null)
					{
						Console.Out.WriteLine(exception.StackTrace);
						sw.WriteLine(exception.StackTrace);
					}
					//sw.Close();
                    sw.Flush();
				}
			}
		}

        protected string testDateFormat()
        {
            DateTime nowDateStr = DateTime.Now;
            string s = "";
            Console.WriteLine("Standard DateTime Format Specifiers");
            s = String.Format(
                "(d) Short date: . . . . . . . {0:d}\n" +
                "(D) Long date:. . . . . . . . {0:D}\n" +
                "(t) Short time: . . . . . . . {0:t}\n" +
                "(T) Long time:. . . . . . . . {0:T}\n" +
                "(f) Full date/short time: . . {0:f}\n" +
                "(F) Full date/long time:. . . {0:F}\n" +
                "(g) General date/short time:. {0:g}\n" +
                "(G) General date/long time: . {0:G}\n" +
                "    (default):. . . . . . . . {0} (default = 'G')\n" +
                "(M) Month:. . . . . . . . . . {0:M}\n" +
                "(R) RFC1123:. . . . . . . . . {0:R}\n" +
                "(s) Sortable: . . . . . . . . {0:s}\n" +
                "(u) Universal sortable: . . . {0:u} (invariant)\n" +
                "(U) Universal sortable: . . . {0:U}\n" +
                "(Y) Year: . . . . . . . . . . {0:Y}\n",
                nowDateStr);
            Console.WriteLine(s);
            return s;
        }

		/// <summary>
		///	Just returns this logger (<c>ConsoleLogger</c> is not hierarchical).
		/// </summary>
		/// <param name="name">Ignored</param>
		/// <returns>This ILogger instance.</returns> 
		public ILogger CreateChildLogger(string name )
		{
			return this;
		}
	}
}
