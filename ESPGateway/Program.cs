﻿/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 using System;
using Genesyslab.Platform.ApplicationBlocks.ConfigurationObjectModel;
using Genesyslab.Platform.ApplicationBlocks.ConfigurationObjectModel.CfgObjects;
using Genesyslab.Platform.ApplicationBlocks.ConfigurationObjectModel.Queries;
using Genesyslab.Platform.AppTemplate.Configuration;
using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.Configuration.Protocols;
using Genesyslab.Platform.Configuration.Protocols.Types;

namespace ESPGateway
{
    class Program
    {
        static void Main(string[] args)
        {
            KeyValueCollection config = null;
            bool localCfg = false;

            foreach(string arg in args)
            {
                if ( arg.ToUpper().Equals("-LOCAL") )
                {
                    localCfg = true;
                }
            }

            if ( localCfg == true )
            {
                config = ReadLocalConfig();
            }
            else
            {
                config = ReadGenesysApplicationConfiguration(args);
            }

            ESPServer server = new ESPServer(config);
            server.Initialize();
            server.Run();
            server.Terminate();
        }

        static KeyValueCollection ReadLocalConfig()
        {
            KeyValueCollection config = new KeyValueCollection();

            config.Add("server-port", Properties.Settings.Default.Port);

            KeyValueCollection kvcSettings = new KeyValueCollection();
            config.Add("settings", kvcSettings);
            kvcSettings.Add("extensions-dir", Properties.Settings.Default.ExtensionsDir);

            KeyValueCollection kvcLog = new KeyValueCollection();
            config.Add("log", kvcLog);
            kvcLog.Add("all", Properties.Settings.Default.LogPath);
            kvcLog.Add("verbose", Properties.Settings.Default.LogLevel);

            return config;
        }

        static KeyValueCollection ReadGenesysApplicationConfiguration(string[] args)
        {
            string cfgAppName = "";
            string cfgHost = "";
            int cfgPort = 2020;
            string cfgUsername = "";
            string cfgPassword = "";

            for( int i = 0; i < args.Length; i++)
            {
                switch (args[i].ToUpper())
                {
                    case "-APP": cfgAppName = args[++i]; Console.WriteLine("APP=" + cfgAppName); break;
                    case "-HOST": cfgHost = args[++i]; Console.WriteLine("HOST=" + cfgHost); break;
                    case "-PORT": Int32.TryParse(args[++i], out cfgPort); Console.WriteLine("PORT=" + cfgPort); break;
                    case "-USER": cfgUsername = args[++i]; Console.WriteLine("USER=" + cfgUsername); break;
                    case "-PASS": cfgPassword = args[++i]; Console.WriteLine("PASS=" + cfgPassword); break;
                }
            }

            // Initialize ConfService:
            ConfServerProtocol protocol;
            IConfService cfgService;

            Console.WriteLine("Configuring ConfServerProtocol");

            protocol = new ConfServerProtocol(new Endpoint(cfgAppName, cfgHost, cfgPort));
            protocol.ClientApplicationType = (int)CfgAppType.CFGGenericServer;
            protocol.ClientName = cfgAppName;
            protocol.UserName = cfgUsername;
            protocol.UserPassword = cfgPassword;

            Console.WriteLine("Opening protocol");
            cfgService = ConfServiceFactory.CreateConfService(protocol);
            protocol.Open();

            Console.WriteLine("Retrieving application configuration");
            CfgApplication cfgApplication = cfgService.RetrieveObject<CfgApplication>(new CfgApplicationQuery() { Name = cfgAppName });

            Console.WriteLine("Setting up GCOMApplicationConfiguration");
            GCOMApplicationConfiguration appConfiguration = new GCOMApplicationConfiguration(cfgApplication);

            // Deinitialize ConfService:
            protocol.Close();
            ConfServiceFactory.ReleaseConfService(cfgService);
            cfgService = null;

            Console.WriteLine("server-port: " + appConfiguration.ServerInfo.Port);
            appConfiguration.Options.Add("server-port", appConfiguration.ServerInfo.Port);

            Console.WriteLine("Options: " + appConfiguration.Options.ToString());

            return appConfiguration.Options;
        }
    }
}
