﻿/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Logging;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.OpenMedia.Protocols.ExternalService.Event;
using Genesyslab.Platform.OpenMedia.Protocols.ExternalService.Request;

namespace ESPGatewayExtension
{
    public class ESPExtension : IESPExtension
    {
        protected static string ESP_SERVICE_KEY = "Service";
        protected static string ESP_METHOD_KEY = "Method";
        protected static string ESP_PARAMETERS_KEY = "Parameters";

        protected KeyValueCollection appConfiguration;

        protected string espServiceName;
        protected string espMethodName;
        protected IRequestContext requestContext;
        protected Request3rdServer espRequest;
        protected KeyValueCollection espParameters;
        protected KeyValueCollection espUserdata;

        virtual public void Initialize(KeyValueCollection appConfiguration)
        {
            this.appConfiguration = appConfiguration;
        }

        virtual public IESPExtension HandleRequest(string service, string method, IRequestContext requestContext)
        {
            Logger.Debug("ESPExtension: Handle Request");
            Logger.Debug(requestContext.ToString());

            this.espServiceName = service;
            this.espMethodName = method;
            this.requestContext = requestContext;

            Request3rdServer espReq = requestContext.RequestMessage as Request3rdServer;
            espParameters = espReq.Request.GetAsKeyValueCollection("Parameters");
            if ( espParameters == null )
            {
                espParameters = new KeyValueCollection();
            }
            espUserdata = espReq.UserData;

            // This single ESPExtension will handle all ESP requests!
            // If a unique class instance should handle each request, then
            // this behaviour can be overridden and implemented in a subclass.
            return this;
        }

        virtual public void Terminate()
        {
        }

        virtual public ILogger Logger
        {
            get
            {
                return ESPExtensionLogger.Logger;
            }
        }

        protected void SendError(IRequestContext request, string faultCode, string errrorMessage)
        {
            try
            {
                Request3rdServer espReq = request.RequestMessage as Request3rdServer;

                Event3rdServerFault eventError = Event3rdServerFault.Create(new KeyValueCollection());
                eventError.ReferenceId = espReq.ReferenceId;
                eventError.Request.Add(ESP_SERVICE_KEY, espReq.Request[ESP_SERVICE_KEY]);
                eventError.Request.Add(ESP_METHOD_KEY, espReq.Request[ESP_METHOD_KEY]);

                KeyValueCollection param = new KeyValueCollection();
                param.Add("FaultCode", faultCode);
                param.Add("FaultString", errrorMessage);

                eventError.Request.Add(ESP_PARAMETERS_KEY, param);

                Logger.Info("Sending EventError: " + eventError.ToString());

                request.Respond(eventError);
            }
            catch (Exception e)
            {
                Logger.Debug("Exception Sending EventError: " + e.Message);
            }
        }

        protected void SendOk(IRequestContext request)
        {
            try
            {
                Request3rdServer espReq = request.RequestMessage as Request3rdServer;

                Event3rdServerResponse eventOk = Event3rdServerResponse.Create(new KeyValueCollection());

                eventOk.ReferenceId = espReq.ReferenceId;
                eventOk.Request.Add(ESP_SERVICE_KEY, espReq.Request[ESP_SERVICE_KEY]);
                eventOk.Request.Add(ESP_METHOD_KEY, espReq.Request[ESP_METHOD_KEY]);

                Logger.Info("Sending EventOK: " + eventOk.ToString());

                request.Respond(eventOk);
            }
            catch (Exception e)
            {
                Logger.Debug("Exception Sending EventOK: " + e.Message);
            }
        }
    }
}
