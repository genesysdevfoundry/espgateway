# ESPGateway #

A .NET-based C# application that implements a Genesys External Service Process (ESP) for routing.  The ESP implements Microsoft's Managed Extensibility Framework (MEF) to dynamically load "ESPExtensions".  The project includes a SampleExtension as well as a few sample ChatBotExtensions.  If you develop additional extensions, feel free to contribute back to the project.

### How do I get set up? ###

* Check out the Articles section of the Genesys DevFoundry (https://developer.genesys.com)

### To report issues or ask questions? ###

* Post to the Questions section of the Genesys DevFoundry (https://developer.genesys.com)