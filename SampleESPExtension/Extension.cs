﻿/*
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using System;
using System.ComponentModel.Composition;
using ESPGatewayExtension;
using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Commons.Protocols;

namespace SampleESPExtension
{
    [Export(typeof(IESPExtension))]
    [ExportMetadata("Service", "SampleExtension")]    // <- This is your "Service" name for the ESP routing block
    public class Extension : ESPExtension
    {
        public override void Initialize(KeyValueCollection appConfiguration)
        {
            Logger.Info("SampleExtension: Initializing");
            base.Initialize(appConfiguration);
            Logger.Info("SampleExtension: Initialized");
        }

        public override IESPExtension HandleRequest(String service, String method, IRequestContext requestContext)
        {
            Logger.Info("SampleExtension: Handling ESP request for service:'" + service + "'" + " method:'" + method + "'.");

            try
            {
                // This single ESPExtension will handle all ESP requests!
                // If a unique class instance should handle each request, then
                // this behaviour can be overridden and implemented in a subclass.
                base.HandleRequest(service, method, requestContext);


                SendOk(requestContext);
            }
            catch(Exception exc)
            {
                Logger.Error("Exception Handling Request in SampleExtension: " + exc.Message);

                SendError(requestContext, "100", exc.Message);
            }

            return this;
        }

        public override void Terminate()
        {
            Logger.Info("SampleExtension: Terminating");

            Logger.Info("SampleExtension: Terminated");
        }
    }
}
